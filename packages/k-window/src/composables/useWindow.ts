import { ComputedRef, InjectionKey, Ref } from "vue";
import type { GroupItemProvide } from "./useGroup";

type WindowProvide = {
  transition: ComputedRef<undefined | string>;
  transitionCount: Ref<number>;
  transitionHeight: Ref<undefined | string>;
  isReversed: Ref<boolean>;
  rootRef: Ref<HTMLElement | undefined>;
};

export const WindowSymbol: InjectionKey<WindowProvide> =
  Symbol.for("eka:k-window");
export const WindowGroupSymbol: InjectionKey<GroupItemProvide> =
  Symbol.for("eka:k-window-group");
