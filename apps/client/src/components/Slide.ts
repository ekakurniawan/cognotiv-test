import { defineComponent, h } from "vue";
import { KWindowItem } from "@kurnia1/k-window";

export default defineComponent({
  props: {
    value: {
      type: String,
      required: true,
    },
  },
  setup(props, { slots }) {
    return () =>
      h(
        KWindowItem,
        {
          value: props.value,
          transition: "slide-x-transition",
          reverseTransition: "slide-x-reverse-transition",
          class: "h-full w-full",
        },
        slots
      );
  },
});
