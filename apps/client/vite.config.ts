import { resolve } from "path";
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

const root = resolve(__dirname, "src");
const components = resolve(root, "components");
const pages = resolve(root, "pages");

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      path: "path-browserify",
      "@src": root,
      "@components": components,
      "@pages": pages,
    },
  },
});
