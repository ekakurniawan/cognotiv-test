import type { Config } from "tailwindcss";

export default {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      display: ["Montserrat", "sans-serif"],
    },
    extend: {
      colors: {
        primary: "#FFB800",
      },
      transitionTimingFunction: {
        "in-expo": "cubic-bezier(.65,.05,.05,.65)",
        "out-expo": "cubic-bezier(0.19, 1, 0.22, 1)",
        bounce: "cubic-bezier(1, -0.2, 0, 1.2)",
      },
      animation: {
        fill: "fill .7s ease-in-out infinite alternate",
      },
      keyframes: {
        fill: {
          "0%": { width: "100%" },
          "35%": { width: "100%" },
          "65%": { width: "0%" },
          "100%": { width: "0%" },
        },
      },
    },
  },
  plugins: [],
} satisfies Config;
